<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location: ../index.php");
	exit;   
}
if ($_SESSION['level'] !== 'guru') {
	header("Location: ../index.php");
	exit;
}

    // mengimport koneksi database ($con)
    require '../config/db.php';

    // mengimport user-defined functions
    include '../config/function.php';


    // memastikan URL valid
    if (!isset($_GET['kode']) || empty($_GET['kode'])) {
        $_SESSION['alert'] = array(
            'error' => TRUE,
            'message' => "URL tidak valid."
        );
        // mengarahkan kembali ke halaman utama admin
        header("location: ../kelas.php");
        exit;
    }


    // mendapatkan url dari laman saat ini

    // jika sesi admin tidak aktif, mengarahkan ke halaman utama admin.
    if (!isset($_SESSION['login']) && !$_SESSION['level']) {
        header("location: ../kelas.php?redirect=$urlOfThisPage");
        exit;
    }


    // mendapatkan kode dari pertemuan yang akan diakses
    $kodeMeeting = $_GET['kode'];

    // mengeksekusi query untuk mendapatkan data pertemuan
    // $meetingResult = $con->query("SELECT * FROM Pertemuan WHERE kode='$kodeMeeting'");
    $meetingResult = query("SELECT * FROM tb_kelas INNER JOIN tb_guru ON tb_kelas.pengajar = tb_guru.id_guru where kode_kelas = '$kodeMeeting' ");

    if (sizeof($meetingResult) !== 1) {
        // jika data kelas tidak ditemukan pada database
        if (sizeof($meetingResult) === 0) {
            $_SESSION['alert'] = array(
                'error' => TRUE,
                'message' => "Kelas dengan kode <b>$kodeMatkul</b> tidak dapat ditemukan."
            );
        } else {
            // memberikan respon gagal lainnya
            $_SESSION['alert'] = array(
                'error' => TRUE,
                'message' => "Terjadi kesalahan! <i>$con->error</i>"
            );
        }
        // mengarahkan ke halaman utama admin
        // header("location: kelas.php");
        exit;
    }

    $meetingResult = (sizeof($meetingResult)===1) ? $meetingResult[0] : null;

    // mendapatkan data pertemuan
    // $waktuAkses = explode(' ', $pertemuan['waktu_akses']);
    // $tglAkses = $waktuAkses[0];
    // $jamAkses = substr($waktuAkses[1], 0, -3);


    // menangani form hapus pertemuan
    if (isset($_POST['hapus_pertemuan'])) {
        $kodePertemuan = htmlspecialchars($_POST['kode_pertemuan']);
        $kodeKelas = htmlspecialchars($_POST['kode_kelas']);

        $deleteRespons = $con->query("DELETE FROM Pertemuan WHERE kode='$kodePertemuan'");

        if ($deleteRespons) {
            $_SESSION['alert'] = array(
                'error' => FALSE,
                'message' => "Data Pertemuan berhasil dihapus."
            );
            header("location: ./kelas.php?kode=$kodeKelas");
            exit;

        } else {
            // memberikan respon gagal
            $_SESSION['alert'] = array(
                'error' => TRUE,
                'message' => "Terjadi kesalahan saat menghapus data.<br><i>$con->error!</i>"
            );
        }
    }



   // menangani form ubah Kelas
if (isset($_POST['ubah_matkul'])) {
    $kode = htmlspecialchars(strtoupper($meetingResult['kode_kelas']));
    $nama = htmlspecialchars($_POST['nama']);
    $kodeDosen1 = htmlspecialchars($_POST['pengajar']);

    $stmt = $con->prepare("UPDATE tb_kelas SET nm_kelas = ?, pengajar = ? WHERE kode_kelas = ?");
    $stmt->bind_param('sss', $nama,  $kodeDosen1,  $kode);

    // mengeksekusi query & memberikan respons
    if ($stmt->execute()) {
        $_SESSION['alert'] = array(
            'error' => FALSE,
            'message' => "Perubahan berhasil dilakukan."
        );
    } else {
        $_SESSION['alert'] = array(
            'error' => TRUE,
            'message' => "Terjadi kesalahan saat merubah data.<br><i>$con->error!</i>"
        );
    }

    // memuat ulang halaman agar perubahan dapat dimunculkan
    header("location: pertemuan.php");
    exit;
}
    function code() {
        // Huruf yang sudah ditentukan
        $prefix = "MTR";
    
        // Generate 5 angka acak
        $random_numbers = sprintf("%05d", mt_rand(1, 99999));
    
        // Gabungkan huruf dan angka
        $code = $prefix . $random_numbers;
    
        return $code;
    }


    // menghandle form upload materi
    if (isset($_POST['upload_materi'])) {
        $judul = htmlspecialchars($_POST['judul']);
        $deskripsi = htmlspecialchars($_POST['deskripsi']);
        $jenisMateri = htmlspecialchars($_POST['jenis_materi']);
        $kode = '';

        // mencari kode yang belum terpakai
        do {
            $kode = code_generator(5, 'MTR');
            $checkPK = $con->query("SELECT * FROM Materi WHERE kode='$kode'");
        } while ($checkPK !== FALSE && $checkPK->num_rows > 0);

        $uploadRespons = FALSE;

        if ($jenisMateri === 'url') {
            $url = htmlspecialchars($_POST['url']);
            $uploadRespons = $con->query("INSERT INTO Materi (kode, pertemuan, judul, deskripsi, `url`)
                VALUES ('$kode', '$kodeMeeting', '$judul', '$deskripsi', '$url')");

        } elseif ($jenisMateri === 'file') {
            // mendapatkan nama file & ekstensinya
            $breakFileName = break_filename($_FILES['file_materi']);
            $fileName = $breakFileName['name'];
            $fileExt = $breakFileName['ext'];
            $newFileName = $kode.'_'.$fileName.'.'.$fileExt;    // nama file baru
            $fileDestination = '../db/'.$newFileName;           // lokasi tujuan penyimpanan file

            // mengupload file materi
            $fileUpload = upload_file($_FILES['file_materi'], $fileDestination);
            
            $mimetype = 'application/octet-stream';

            if ($fileUpload['error'] === false) {
                // Mendapatkan tipe mime dari file yang diunggah
                $mimetype = mime_content_type($fileDestination);
    
                $uploadRespons = $con->query("INSERT INTO Materi (kode, pertemuan, judul, deskripsi, nama_file, mimetype)
    VALUES ('$kode', '$kodeMeeting', '$judul', '$deskripsi', '$newFileName','$mimetype')");

            } else {
                $_SESSION['alert'] = array(
                    'error' => $fileUpload['error'],
                    'message' => $fileUpload['message']
                );
            }
        }
        // memberikan respon berhasil
        if ($uploadRespons) {
            $_SESSION['alert'] = array(
                'error' => false,
                'message' => "File Materi berhasil ditambahkan."
            );
        }
        header("location: pertemuan.php?kode=". $meetingResult['kode_kelas']);
        exit;
    }


    // menghandle form buat tugas baru
    if (isset($_POST['buat_tugas'])) {
        $judul = htmlspecialchars($_POST['judul']);
        $deskripsi = htmlspecialchars($_POST['deskripsi']);
        $tglDeadline = htmlspecialchars($_POST['tgl_deadline']);
        $jamDeadline = htmlspecialchars($_POST['jam_deadline']);
        $deadline = $tglDeadline.' '.$jamDeadline;
        $lampiran = $_FILES['lampiran'];
        $kode = '';

        // mencari kode yang belum terpakai
        do {
            $kode = code_generator(5, 'TGS');
            $checkPK = $con->query("SELECT * FROM Tugas WHERE kode='$kode'");
        } while ($checkPK !== FALSE && $checkPK->num_rows > 0);

        $uploadRespons = FALSE;

        // mengecek jika tidak ada file lampiran yang diupload
        $emptyFileErrCode = 4;
        if ($lampiran['error'] === $emptyFileErrCode) {
            // mengupload tugas tanpa lampiran
            $uploadRespons = $con->query("INSERT INTO Tugas (kode, pertemuan, judul, deskripsi, deadline)
                VALUES ('$kode', '$kodeMeeting', '$judul', '$deskripsi', '$deadline')");

        } else {
            // mendapatkan nama file & ekstensinya
            $breakFileName = break_filename($lampiran);
            $fileName = $breakFileName['name'];
            $fileExt = $breakFileName['ext'];
            $newFileName = $kode.'_'.$fileName.'.'.$fileExt;    // nama file baru
            $fileDestination = '../db/'.$newFileName;           // lokasi tujuan penyimpanan file

            // mengupload file lampiran
            $fileUpload = upload_file($lampiran, $fileDestination);

            $mimetype = 'application/octet-stream';
            if ($fileUpload['error'] === false) {
                // menyimpan data tugas baru beserta nama file lampirannya 
                $mimetype = mime_content_type($fileDestination);
                $uploadRespons = mysqli_query($con, "INSERT INTO Tugas (kode, pertemuan, judul, deskripsi, deadline, lampiran, mimetype)
                    VALUES ('$kode', '$kodeMeeting', '$judul', '$deskripsi', '$deadline', '$newFileName', '$mimetype')");

            } else {
                $_SESSION['alert'] = array(
                    'error' => $fileUpload['error'],
                    'message' => $fileUpload['message']
                );
            }
        }
        // memberikan respon berhasil
        if ($uploadRespons) {
            $_SESSION['alert'] = array(
                'error' => FALSE,
                'message' => "Tugas baru berhasil dibuat."
            );
        }
        header("location: pertemuan.php?kode=". $meetingResult['kode_kelas']);
        exit;
    }


    // menghandle form buat ujian baru
    if (isset($_POST['buat_ujian'])) {
        $durasi = htmlspecialchars($_POST['durasi']);
        $catatan = htmlspecialchars($_POST['catatan']);
        $kode = '';

        // mencari kode yang belum terpakai
        do {
            $kode = code_generator(5, 'UJI');
            $checkPK = $con->query("SELECT * FROM Ujian WHERE kode='$kode'");
        } while ($checkPK !== FALSE && $checkPK->num_rows > 0);

        $insertRespons = $con->query("INSERT INTO Ujian (kode, pertemuan, durasi, catatan)
            VALUES ('$kode', '$kodeMeeting', '$durasi', '$catatan')");

        if ($insertRespons) {
            $_SESSION['alert'] = array(
                'error' => FALSE,
                'message' => "Ujian berhasil dibuat dengan durasi pengerjaan selama $durasi."
            );
        } else {
            // memberikan respon gagal
            $duplicatePKErrCode = 1062;
            if ($con->errno === $duplicatePKErrCode) {
                $_SESSION['alert'] = array(
                    'error' => TRUE,
                    'message' => "Kode Ujian <b>$kode</b> sudah terpakai! Harap gunakan kode lain."
                );
            } else {
                $_SESSION['alert'] = array(
                    'error' => TRUE,
                    'message' => "Terjadi kesalahan saat membuat ujian.<br><i>$con->error!</i>"
                );
            }
        }
        header("location: $urlOfThisPage");
        exit;
    }


    // menangani form daftar kehadiran mahasiswa
    if (isset($_POST['simpan_presensi'])) {
        $kodeArr = $_POST['kode'];
        $kehadiranArr = $_POST['kehadiran'];
        $keteranganArr = $_POST['keterangan'];

        $countErrors = 0;
        $i = 0;
        foreach ($kodeArr as $kode) {
            $updateRespons = $con->query("UPDATE Kehadiran SET hadir=$kehadiranArr[$i], keterangan='$keteranganArr[$i]' WHERE kode='$kode'");
            if ($updateRespons === FALSE) $countErrors++;
            $i++;
        }

        // memberikan respon
        if ($countErrors === 0) {
            $_SESSION['alert'] = array(
                'error' => FALSE,
                'message' => "Data Kehadiran berhasil disimpan seluruhnya."
            );
        } else {
            $_SESSION['alert'] = array(
                'error' => TRUE,
                'message' => "Terjadi kesalahan saat mengupdate $countErrors baris!"
            );
        }
        header("location: $urlOfThisPage");
        exit;
    }


    // mengeksekusi query untuk mendapatkan data kelas
    // $kodeKelas = $pertemuan['kelas'];
    // $klsResult = $con->query("SELECT * FROM Kelas WHERE kode='$kodeKelas'");

    // if ($klsResult && $klsResult->num_rows !== 1) {
    //     if ($klsResult->num_rows === 0) {
    //         // jika data kelas tidak ditemukan pada database
    //         $_SESSION['alert'] = array(
    //             'error' => TRUE,
    //             'message' => "Data kelas dengan kode <b>'$kodeKelas'</b> tidak dapat ditemukan"
    //         );
    //     } else {
    //         // memberikan respon gagal lainnya
    //         $_SESSION['alert'] = array(
    //             'error' => TRUE,
    //             'message' => "Terjadi kesalahan! <i>$con->error</i>"
    //         );
    //     }
    //     // mengarahkan kembali ke halaman utama admin
    //     header("location: ../admin.php");
    //     exit;
    // }

    // // mendapatkan data kelas
    // $kelas = $klsResult->fetch_assoc();

    // // mengeksekusi query untuk mendapatkan data mata kuliah dari pertemuan ini
    // $kodeMK = $kelas['mata_kuliah'];
    // $mkResult = $con->query("SELECT * FROM Mata_Kuliah WHERE kode='$kodeMK'");

    // if ($mkResult && $mkResult->num_rows !== 1) {
    //     if ($mkResult->num_rows === 0) {
    //         // jika data mata kuliah tidak ditemukan pada database
    //         $_SESSION['alert'] = array(
    //             'error' => TRUE,
    //             'message' => "Data mata kuliah yang terkait dengan pertemuan ini tidak dapat ditemukan"
    //         );
    //     } else {
    //         // memberikan respon gagal lainnya
    //         $_SESSION['alert'] = array(
    //             'error' => TRUE,
    //             'message' => "Terjadi kesalahan! <i>$con->error</i>"
    //         );
    //     }
    //     // mengarahkan kembali ke halaman utama admin
    //     header("location: ../admin.php");
    //     exit;
    // }

    // mendapatkan data mata kuliah
    // $matkul = $mkResult->fetch_assoc();


    // // mengecek jika ada suatu peringatan (alert)
    // $alert = '';
    // if (isset($_SESSION['alert']) && !empty($_SESSION['alert'])) {
    //     $alert = array(
    //         'error' => $_SESSION['alert']['error'],
    //         'message' => $_SESSION['alert']['message']
    //     );
    //     $_SESSION['alert'] = '';
    // }

    $title = "Pembelajaran";
    require_once "header.php";
    require_once "navbar.php";
    require_once "sidebar.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pertemuan <?=$pertemuan['nomor_pert']?> Kelas <?=$kelas['nama']?></title>
</head>
<body>
    <!-- <?php if ($alert) : ?>
        <?php if($alert['error']) : ?>
            <div class="alert alert-warning alert-dismissible fade show position-absolute top-2 start-50 translate-middle-x" role="alert">
                <?=$alert['message']?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php else : ?>
            <div class="alert alert-success alert-dismissible fade show position-absolute top-2 start-50 translate-middle-x" role="alert">
                <?=$alert['message']?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif; ?>
    <?php
        $alert = '';
        endif;
    ?> -->
    <div id="layoutSidenav_content">
	<div class="container-fluid px-4">
    <!-- <h1 class="mt-4">Daftar Kelas</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="admin.php">Beranda</a></li>
				<li class="breadcrumb-item active" aria-current="page">Pembelajaran</li>
			</ol>
		</nav> -->
    <main class="container-md">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <!-- <li class="breadcrumb-item"><a href="../admin.php">Beranda</a></li> -->
                <!-- <li class="breadcrumb-item"><a href="./matkul.php?kode=<?=$matkul['kode']?>"><?=$matkul['nama']?></a></li> -->
                <!-- <li class="breadcrumb-item"><a href="./kelas.php?kode=<?=$kelas['kode']?>">Kelas <?=$kelas['nama']?></a></li> -->
                <!-- <li class="breadcrumb-item active" aria-current="page">Pertemuan <?=$pertemuan['nomor_pert']?></li> -->
            </ol>
        </nav>
        <section class="mb-4">
        <h1><?=$meetingResult['kode_kelas']?></h1>
            <h2><?=$meetingResult['nm_kelas']?></h2>
        </section>
        <nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="admin.php">Beranda</a></li>
				<li class="breadcrumb-item active" aria-current="page">Pembelajaran</li>
			</ol>
		</nav>
        <section class="mb-5">
            <div class="accordion" id="menuPanel">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button d-inline-flex gap-2 align-items-center collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <!-- <span class="material-icons">description</span> -->
                            <span>Detail Pertemuan</span>
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#menuPanel">
                        <div class="accordion-body">
                            <table class="table">
                                <tr>
                                    <th>Kode Kelas</th>
                                    <td><?=$meetingResult['kode_kelas']?></td>
                                </tr>
                                <tr>
                                    <th>Nama kelas</th>
                                    <td><?=$meetingResult['nm_kelas']?></td>
                                </tr>
                                <tr>
                                    <th>Pengajar</th>
                                    <td><?=$meetingResult['pengajar']?></td>
                                </tr>
                            </table>
                            <hr>
                            <form action="./pertemuan.php?kode=<?=$pertemuan['kode']?>" method="post">
                                <input type="hidden" name="kode_pertemuan" value="<?=$pertemuan['kode']?>">
                                <input type="hidden" name="kode_kelas" value="<?=$kelas['kode']?>">
                                <button class="btn btn-danger d-inline-flex align-items-center gap-1" type="submit" name="hapus_pertemuan" onclick="return confirm('Anda yakin ingin menghapus Pertemuan ini beserta seluruh isinya (seperti materi, tugas, dll) ?')">
                                    <span class="material-icons">delete</span>
                                    <span>Hapus Pertemuan & Seluruh Isinya</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                        <button class="accordion-button d-inline-flex gap-2 align-items-center collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <!-- <span class="material-icons"></span> -->
                            <span>Ubah Pertemuan</span>
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#menuPanel">
                        <div class="accordion-body">
                            <p>Silahkan ubah data di bawah ini jika memang diperlukan perubahan, lalu klik tombol <b>"Simpan Perubahan"</b>.</p>
                            <hr>
                            <form action="./pertemuan.php?kode=<?=$kodeMeeting?>" method="post">
                                <article class="mb-3">
                                    <label class="form-label" for="kodePertemuan">Kode Pertemuan :</label>
                                    <input class="form-control" type="text" name="kode_kelas" id="kodePertemuan" value="<?=$meetingResult['kode_kelas']?>" required disabled>
                                </article>
                                <article class="mb-3">
                                    <label class="form-label" for="kelas">Kelas :</label>
                                    <input class="form-control" type="text" name="nama" id="kelas" value="<?=$meetingResult['nm_kelas']?>" required>
                                </article>
                                <?php
                                //query untuk mendapatkan semua data dosen yang ada
                                $dosenResult = $con->query("SELECT id_guru, nm_guru FROM tb_guru");
                                $listDosen = array();


                                while ($dosen = $dosenResult->fetch_assoc()) {
                                    array_push($listDosen, $dosen);
                                }
                                ?>
                                <article class="mb-3">
                                    <label class="form-label" for="dosenPengampu1">Guru Pengajar :</label>
                                    <select class="form-select" name="pengajar" id="dosenPengampu1" required>
                                        <option value="" disabled>-- Pilih Pengajar --</option>
                                        <?php if (sizeof($listDosen) > 0) : ?>
                                            <?php foreach ($listDosen as $dosen) : ?>
                                                <?php if ($dosen['kode_kelas'] === $matkul['id_guru']) : ?>
                                                    <option value="<?= $dosen['id_guru'] ?>" selected><?= $dosen['id_guru'] ?> - <?= $dosen['nm_guru'] ?></option>
                                                <?php else : ?>
                                                    <option value="<?= $dosen['id_guru'] ?>"><?= $dosen['id_guru'] ?> - <?= $dosen['nm_guru'] ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                    <div class="invalid-feedback">Pilih salah satu dari opsi yang tersedia.</div>
                          </article>
                                <article class="mb-3 d-flex gap-2">
                                    <button id="tambahBrg" type="submit" name="ubah_pertemuan" class="btn btn-success flex-fill d-flex align-items-center justify-content-center gap-1">
                                        <!-- <span class="material-icons">save</span> -->
                                        <span>Simpan Perubahan</span>
                                    </button>
                                </article>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button d-inline-flex gap-2 align-items-center collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            <!-- <span class="material-icons">upload_file</span> -->
                            <span>Upload Materi Baru</span>
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#menuPanel">
                        <div class="accordion-body">
                           <p>Silahkan <b>lengkapi kolom-kolom di bawah ini</b> sesuai petunjuk yang tersedia.</p>
                            <hr>
                            <form action="./pertemuan.php?kode=<?=$meetingResult['kode_kelas']?>" method="post" enctype="multipart/form-data">
                                <article class="mb-3">
                                    <label class="form-label" for="judul">Judul :</label>
                                    <input class="form-control" type="text" name="judul" id="judul" required>
                                    <div class="invalid-feedback">Judul harus tidak boleh kosong.</div>
                                    <div class="form-text">Masukkan judul dari materi yang ingin Anda upload.</div>
                                </article>
                                <article class="mb-3">
                                    <label class="form-label" for="deskripsi">Deskripsi (opsional) :</label>
                                    <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="1"></textarea>
                                    <div class="form-text">Masukkan deskripsi mengenai materi yang ingin Anda upload.</div>
                                </article>
                                <article class="mb-3">
                                    <label for="jenisMateri" class="form-label">Jenis Materi :</label>
                                    <select class="form-select" name="jenis_materi" id="jenisMateri" required>
                                        <option value="" selected disabled>-- Pilih Jenis Materi --</option>
                                        <option value="file">File</option>
                                        <option value="url">URL</option>
                                    </select>
                                    <div class="form-text">Pilih salah satu jenis materi yang tersedia.</div>
                                </article>
                                <article class="input-materi mb-3 d-none" data-type="file">
                                    <label for="fileMateri" class="form-label">Upload File :</label>
                                    <div class="input-group">
                                        <input class="form-control" type="file" name="file_materi" id="fileMateri">
                                        <div class="input-group-text" title="Ukuran file maksimal yang diperbolehkan">Maks. 5 MB</div>
                                    </div>
                                    <div class="form-text">
                                        Pilih file materi yang ingin Anda upload.<br>
                                        <b>Ekstensi yang diperbolehkan: 'jpg', 'jpeg', 'png', 'pdf', 'pptx', 'docx', 'zip', 'rar'</b>
                                    </div>
                                </article>
                                <article class="input-materi mb-3 d-none" data-type="url">
                                    <label class="form-label" for="url">URL atau Link :</label>
                                    <div class="input-group">
                                        <input class="form-control" type="url" name="url" id="url">
                                        <div class="input-group-text" title="Contoh URL atau link">https://url.com/contoh</div>
                                    </div>
                                    <div class="form-text">Masukkan URL / Link materi seperti: link Youtube, dll.</div>
                                </article>

                                <article class="mb-3 d-flex gap-2">
                                    <button id="tambahBrg" type="submit" name="upload_materi" class="btn btn-success flex-fill d-flex align-items-center justify-content-center gap-1">
                                        <!-- <span class="material-icons">upload_file</span> -->
                                        <span>Upload Materi Baru</span>
                                    </button>
                                </article>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingFour">
                        <button class="accordion-button d-inline-flex gap-2 align-items-center collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            <!-- <span class="material-icons">add_task</span> -->
                            <span>Buat Tugas Baru</span>
                        </button>
                    </h2>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#menuPanel">
                        <div class="accordion-body">
                            <p>Silahkan <b>lengkapi kolom-kolom di bawah ini</b> sesuai petunjuk yang tersedia.</p>
                            <hr>
                            <form action="pertemuan.php?kode=<?=$meetingResult['kode_kelas']?>" method="post" enctype="multipart/form-data">
                                <article class="mb-3">
                                    <label class="form-label" for="judul">Judul :</label>
                                    <input class="form-control" type="text" name="judul" id="judul" required>
                                    <div class="invalid-feedback">Judul tidak boleh kosong.</div>
                                    <div class="form-text">Masukkan judul dari tugas yang ingin Anda berikan.</div>
                                </article>
                                <article class="mb-3">
                                    <label class="form-label" for="deskripsi">Deskripsi (opsional) :</label>
                                    <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="1"></textarea>
                                    <div class="form-text">Masukkan deskripsi dari tugas yang ingin Anda berikan.</div>
                                </article>
                                <article class="mb-3">
                                    <label class="form-label" for="tglDeadline">Batas Tanggal Pengumpulan Tugas :</label>
                                    <div class="input-group">
                                        <input class="form-control" type="date" name="tgl_deadline" id="tglDeadline" required>
                                        <span class="input-group-text" title="Tanggal 28 Mei 2021">Contoh: 05/28/2021</span>
                                    </div>
                                    <div class="form-text">
                                        Pilih tanggal kapan batas terakhir pengumpulan tugas bagi mahasiswa.<br>
                                        <b>Format penulisan: [Bulan/Tanggal/Tahun].</b>
                                    </div>
                                </article>
                                <article class="mb-3">
                                    <label class="form-label" for="jamDeadline">Batas Waktu Pengumpulan Tugas :</label>
                                    <div class="input-group">
                                        <input class="form-control" type="time" name="jam_deadline" id="jamDeadline" required>
                                        <span class="input-group-text" title="Pukul 14.30 (Sore)">Contoh: 02:30 PM</span>
                                    </div>
                                    <div class="form-text">
                                        Pilih pukul berapa batas terakhir pengumpulan tugas bagi mahasiswa.<br>
                                        <b>Format penulisan: [Jam:Menit] diikuti AM atau PM.</b><br>
                                        AM = Pagi, PM = Siang/Sore/Malam.
                                    </div>
                                </article>
                                <article class="mb-3">
                                    <label for="lampiran" class="form-label">File Lampiran (Opsional) :</label>
                                    <div class="input-group">
                                        <input class="form-control" type="file" name="lampiran" id="lampiran">
                                        <div class="input-group-text" title="Ukuran file maksimal yang diperbolehkan">Maks. 5 MB</div>
                                    </div>
                                    <div class="form-text">
                                        Silahkan upload file terkait instruksi untuk tugas ini jika diperlukan.<br>
                                        <b>Ekstensi yang diperbolehkan: 'jpg', 'jpeg', 'png', 'pdf', 'pptx', 'docx', 'zip', 'rar'</b>
                                    </div>
                                </article>
                                <article class="mb-3 d-flex gap-2">
                                    <button id="tambahBrg" type="submit" name="buat_tugas" class="btn btn-success flex-fill d-flex align-items-center justify-content-center gap-1">
                                        <span class="material-icons">add_task</span>
                                        <span>Buat Tugas Baru</span>
                                    </button>
                                </article>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
        <section class="mb-5">
            <h3>Daftar Materi</h3>
            <?php
                // mendapatkan data materi dari pertemuan ini
                $listMateri = $con->query("SELECT * FROM materi WHERE pertemuan='$kodeMeeting'");
                if ($errCode = $con->errno !== 0) {
                    $_SESSION['alert'] = array(
                        'error' => TRUE,
                        'message' => "Terjadi kesalahan saat mengambil data materi! <i>$con->error</i> $errCode"
                    );
                }
            ?>
            <?php if ($listMateri && $listMateri->num_rows>0) : ?>
                <div class="responsive-table">
                    <table class="mt-3 table table-bordered table-striped table-hover">
                        <thead class="text-center">
                            <tr>
                                <th scope="col">Kode</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">File / URL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($listMateri as $materi) : ?>
                                <tr class="" data-link="">
                                    <td><?=$materi['kode']?></td>
                                    <td><?=$materi['judul']?></td>
                                    <td><?=$materi['deskripsi']?></td>
                                    <?php if (!empty($materi['nama_file'])) : ?>
                                        <td><a href="../db/<?=$materi['nama_file']?>" target="_blank"><?=$materi['nama_file']?></a></td>
                                    <?php else : ?>
                                        <td><a href="<?=$materi['url']?>" target="_blank"><?=$materi['url']?></a></td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php else : ?>
                <p class="p-3 border bg-light">Anda belum memberikan materi apapun untuk pertemuan ini.</p>
            <?php endif; ?>
        </section>
        <section class="mb-5">
            <h3>Daftar Tugas</h3>
            <?php
                // mendapatkan data tugas dari pertemuan ini
                $tugasResult = $con->query("SELECT * FROM Tugas WHERE pertemuan='$kodeMeeting'");
                if ($errCode = mysqli_errno($con) !== 0) {
                    $_SESSION['alert'] = array(
                        'error' => TRUE,
                        'message' => "Terjadi kesalahan saat mengambil data tugas! <i>$con->error</i> $errCode"
                    );
                }
            ?>
            <?php if ($tugasResult && $tugasResult->num_rows > 0) : ?>
                <div class="responsive-table">
                    <table class="mt-3 table table-bordered table-striped table-hover">
                        <thead class="text-center">
                            <tr>
                                <th scope="col">Kode</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Deadline</th>
                                <th scope="col">File / URL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($tugas = $tugasResult->fetch_assoc()) : ?>
                                <tr class="" data-link="">
                                    <td><?=$tugas['kode']?></td>
                                    <td><?=$tugas['judul']?></td>
                                    <td><?=$tugas['deskripsi']?></td>
                                    <td><?=$tugas['deadline']?></td>
                                    <?php if (!empty($tugas['lampiran'])) : ?>
                                        <td><a href="../db/<?=$tugas['lampiran']?>" target="_blank"><?=$tugas['lampiran']?></a></td>
                                    <?php else : ?>
                                        <td><?=$tugas['lampiran']?></a></td>
                                    <?php endif; ?>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            <?php else : ?>
                <!-- <p class="p-3 border bg-light">Anda belum memberikan tugas apapun untuk pertemuan ini.</p> -->
                <?php endif; ?>
            </section>
        <section class="mb-5">
            <!-- <h3>Daftar Evaluasi</h3> -->
            <?php
                // mendapatkan data ujian dari pertemuan ini
                $ujianResult = $con->query("SELECT * FROM Ujian WHERE pertemuan='$kodeMeeting'");
                if ($errCode = mysqli_errno($con) !== 0) {
                    $_SESSION['alert'] = array(
                        'error' => TRUE,
                        'message' => "Terjadi kesalahan saat mengambil data ujian! <i>$con->error</i> $errCode"
                    );
                }
            ?>
            <?php if ($ujianResult && $ujianResult->num_rows > 0) : ?>
                <div class="responsive-table">
                    <table class="mt-3 table table-bordered table-striped table-hover">
                        <thead class="text-center">
                            <tr>
                                <th scope="col">Kode</th>
                                <th scope="col">Durasi</th>
                                <th scope="col">Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php while ($ujian = $ujianResult->fetch_assoc()) : ?>
                                <tr class="ujian" data-link="./ujian.php?kode=<?=$ujian['kode']?>">
                                    <td><?=$ujian['kode']?></td>
                                    <td class="text-center"><?=$ujian['durasi']?></td>
                                    <td><?=$ujian['catatan']?></td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            <?php else : ?>
                <!-- <p class="p-3 border bg-light">Anda belum memberikan ujian apapun untuk pertemuan ini.</p> -->
            <?php endif; ?>
        </section>
        <section class="mb-5">
            <!-- <h3>Daftar Kehadiran Mahasiswa</h3> -->
            <?php
                // mengeksekusi query untuk mendapatkan data kehadiran dari pertemuan ini
                $listPresensi = call_procedure($con, "absensi_pertemuan('$kodeMeeting')");
                if ($errCode = mysqli_errno($con) !== 0) {
                    $_SESSION['alert'] = array(
                        'error' => TRUE,
                        'message' => "Terjadi kesalahan saat mengambil data presensi! <i>$con->error</i> $errCode"
                    );
                }
            ?>
            <?php if (sizeof($listPresensi) > 0) : ?>
                <form action="./pertemuan.php?kode=<?=$kodeMeeting?>" method="post">
                    <div class="responsive-table">
                        <table class="mt-3 table table-bordered table-striped table-hover">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">NIM</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Hadir</th>
                                    <th scope="col">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($listPresensi as $presensi) : ?>
                                <tr data-id="<?=$presensi['kode']?>">
                                        <td><?=$presensi['nim']?></td>
                                        <td><?=$presensi['nama_lengkap']?></td>
                                        <td>
                                            <div class="form-check d-flex justify-content-center align-items-stretch">
                                                <?php if ($presensi['hadir']) : ?>
                                                    <input class="form-check-input trigger-check" type="checkbox" data-id="<?=$presensi['kode']?>" checked>
                                                    <input class="check-value" type="hidden" name="kehadiran[]" id="hdr_<?=$presensi['kode']?>" data-id="<?=$presensi['kode']?>" value="1">
                                                <?php else : ?>
                                                    <input class="form-check-input trigger-check" type="checkbox" data-id="<?=$presensi['kode']?>">
                                                    <input class="check-value" type="hidden" name="kehadiran[]" id="hdr_<?=$presensi['kode']?>" data-id="<?=$presensi['kode']?>" value="0">
                                                <?php endif; ?>
                                                <input type="hidden" name="kode[]" value="<?=$presensi['kode']?>">
                                            </div>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="keterangan[]" id="ktr_<?=$presensi['kode']?>" value="<?=$presensi['keterangan']?>">
                                        </td>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <button class="btn btn-success" name="simpan_presensi" type="submit">Simpan Presensi</button>
                </form>
            <?php else : ?>
                <p class="p-3 border bg-light">Tidak ditemukan satupun data presensi untuk pertemuan ini.</p>
            <?php endif; ?>
        </section>
        <section>
            <a href="./kelas.php?kode=<?=$kelas['kode']?>" class="mt-3 btn btn-primary d-flex align-items-center justify-content-center gap-2">
                <!-- <span class="material-icons">arrow_back</span> -->
                <span>Kembali</span>
            </a>
        </section>
    </main>
    <script>
        const materiTypes = document.querySelectorAll('#jenisMateri option');
        const materiInputs = document.querySelectorAll('.input-materi');

        materiTypes.forEach((type) => {
            type.addEventListener('click', () => {
                materiInputs.forEach((inputEl) => {
                    inputEl.classList.add('d-none');

                    const tipeMateri = inputEl.getAttribute('data-type');

                    if (type.getAttribute('value') === tipeMateri) {
                        const inputField = inputEl.querySelector('input');

                        inputField.setAttribute('required', '');
                        inputEl.classList.remove('d-none');
                    }
                });
            });
        });

        const allTriggerCheck = document.querySelectorAll('.trigger-check');
        const checkValues = document.querySelectorAll('.check-value');

        allTriggerCheck.forEach((trigger) => {
            trigger.addEventListener('click', () => {
                checkValues.forEach((checkValue) => {
                    if (trigger.getAttribute('data-id') == checkValue.getAttribute('data-id')) {
                        if (trigger.checked)
                            checkValue.value = 1;
                        else
                            checkValue.value = 0;
                    }
                });
            });
        });

        const exams = document.querySelectorAll('.ujian');
        exams.forEach((exam) => {
            exam.addEventListener('click', () => {
                window.location.href = exam.getAttribute('data-link');
            })
        });
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Menanggapi perubahan pada dropdown
            document.getElementById("jenisMateri").addEventListener("change", function() {
                // Mendapatkan nilai terpilih
                var selectedValue = this.value;

                // Menampilkan/menyembunyikan elemen formulir sesuai dengan pilihan pengguna
                toggleFormElements(selectedValue);
            });

            // Fungsi untuk menampilkan/menyembunyikan elemen formulir
            function toggleFormElements(selectedValue) {
                // Mendapatkan semua elemen formulir
                var fileInput = document.querySelector(".input-materi[data-type='file']");
                var urlInput = document.querySelector(".input-materi[data-type='url']");

                // Menyembunyikan semua elemen formulir terlebih dahulu
                fileInput.classList.add("d-none");
                urlInput.classList.add("d-none");

                // Menampilkan elemen formulir yang sesuai dengan pilihan pengguna
                if (selectedValue === "file") {
                    fileInput.classList.remove("d-none");
                } else if (selectedValue === "url") {
                    urlInput.classList.remove("d-none");
                }
            }
        });
</script>
        
    <?php require_once "footer.php"; ?>

</body>
</html>
