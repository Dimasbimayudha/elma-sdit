<?php
session_start();
var_dump($_SESSION);

require_once "../config/db.php";

require_once "header.php";
require_once "navbar.php";
require_once "sidebar.php";

// tambah data
if (isset($_POST['bsimpan'])) {
    $password = password_hash(htmlspecialchars($_POST['password']), PASSWORD_BCRYPT);
    $simpan = mysqli_query($con, "INSERT INTO tb_guru (id_guru, nm_guru, username, password, level) 
    VALUES('$_POST[id]', '$_POST[nama]', '$_POST[username]', '$password', '$_POST[level]')");

    if ($simpan) {
        echo "<script> 
        alert('simpan data sukses!!!');
        document.location='data_guru.php';
        </script>";
    } else {
        echo "<script> 
        alert('simpan data gagal!!!');
        document.location='data_guru.php';
        </script>";
    }
}

// edit data
if (isset($_POST['bedit'])) {
    $password = password_hash(htmlspecialchars($_POST['password']), PASSWORD_BCRYPT);
    $edit = mysqli_query($con, "UPDATE tb_guru SET id_guru ='$_POST[id]', nm_guru ='$_POST[nama]', username ='$_POST[username]', password ='$_POST[password]', level ='$_POST[level]' WHERE id_guru ='$_POST[id]' ");

    if ($edit) {
        echo "<script> 
        alert('Ubah data sukses!!!');
        document.location='data_guru.php';
        </script>";
    } else {
        echo "<script> 
        alert('Ubah data gagal!!!');
        document.location='data_guru.php';
        </script>";
    }
}

// hapus data 
if (isset($_POST['bhapus'])) {
    $hapus = mysqli_query($con, "DELETE FROM tb_guru WHERE id_guru ='$_POST[id]' ");

    if ($hapus) {
        echo "<script> 
        alert('hapus data sukses!!!');
        document.location='data_guru.php';
        </script>";
    } else {
        echo "<script> 
        alert('hapus data gagal!!!');
        document.location='data_guru.php';
        </script>";
}
}

?>


<div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Data Guru</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="kelas.php">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data Guru</li>
                        </ol>
                        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Data +
        </button>

        <!-- Modal Tambah Data-->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Guru</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="">
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Id</label>
                                <input type="text" class="form-control" name="id" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Username</label>
                                <input type="text" class="form-control" name="username" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Level</label>
                                <input type="text" class="form-control" name="level" required>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" name="bsimpan" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>

                </div>
            </div>
</div>



                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Data Guru
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>nama</th>
                                            <th>username</th>
                                            <th>level</th>
                                            <th colspan="2">aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>id</th>
                                            <th>nama</th>
                                            <th>username</th>
                                            <th>level</th>
                                            <th colspan="2">aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>   
                                    <?php


$tampilMas    = mysqli_query($con, "SELECT * FROM tb_guru");
while ($mas = mysqli_fetch_array($tampilMas)) :

?>
    <tr>
        <td><?= $mas['id_guru']; ?></td>
        <td><?= $mas['nm_guru']; ?></td>
        <td><?= $mas['username']; ?></td>
        <td><?= $mas['level']; ?></td>
        <td>
            <button type="button" class="btn btn-warning" data-bs-toggle="modal" href="#edit<?= $mas['id_guru']; ?>">
                <i class="fa-solid fa-pen-to-square"></i>Edit
            </button>
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#hapus<?= $mas['id_guru']; ?>">
                <i class="fa-solid fa-trash-can"></i>hapus
            </button>
        </td>



</tr>
            <!-- modal update -->
            <div class="modal fade" id="edit<?= $mas['id_guru']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="edit">Edit Data Guru</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="">
                                                <div class="mb-3">
                                                    <label for="exampleInputEmail1" class="form-label">Id</label>
                                                    <input type="text" class="form-control" name="id" value="<?= $mas['id_guru']; ?>" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="exampleInputPassword1" class="form-label">Nama Lengkap</label>
                                                    <input type="text" class="form-control" name="nama" value="<?= $mas['nm_guru']; ?>" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="exampleInputEmail1" class="form-label">Username</label>
                                                    <input type="text" class="form-control" name="username" value="<?= $mas['username']; ?>" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="exampleInputPassword1" class="form-label">Password</label>
                                                    <input type="password" class="form-control" name="password" value="<?= $mas['password']; ?>" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="exampleInputPassword1" class="form-label">Level</label>
                                                    <input type="text" class="form-control" name="level" value="<?= $mas['level']; ?>" required>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                                            <button type="submit" name="bedit" class="btn btn-primary">Update</button>
                                        </div>
                                        </form>

                                    </div>
                                </div>
                </div>
                <!-- model hapus -->
                <div class="modal fade" id="hapus<?= $mas['id_guru']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5 text-center" id="hapus">Konfirmasi!</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="">

                                                <input type="hidden" class="form-control" name="id" value="<?= $mas['id_guru']; ?>" required>


                                                <h5 class="text-center">Apakah Anda Yakin Ingin Menghapus Data Ini <br>
                                                    <span class="text-danger"> <?= $mas['id_guru']; ?> - <?= $mas['nm_guru']; ?>" </span>
                                                </h5>


                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" name="bhapus" class="btn btn-primary">Hapus</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                </div>
<?php endwhile ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
              






<?php require_once "footer.php"; ?>