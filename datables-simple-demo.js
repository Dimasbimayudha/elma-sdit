window.addEventListener('DOMContentLoaded', event => {
    // simple-datatables
    // https://github.com//fiduswriter/simple-datatables/wiki

    const datatablessimple = document.getElementById('datatablessimple');
    if (datatablessimple) {
        new simpledatatables.datatable(datatablessimple);
    }
})