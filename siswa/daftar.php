<?php
    session_start();
    // var_dump($_SESSION);
    if (!isset($_SESSION['login'])); {
        header("location: ../index.php");
        exit;
    }
    if ($_SESSION['level'] !== 'siswa') {
        header("location: ../index.php");
        exit;
    }

     // mengimport koneksi database ($conn)
     require './config/db.php';

     // mengimport user-defined functions
     include './config/function.php';

    //jika sesi login tidak aktif dan user tidak valid
    if (!isset($_SESSION['login']) || !$_SESSION['login'] || !isset($_SESSION['level']) || empty($_SESSION['level'])) {
        // mengarahkan ke halaman login
        header("location: login.php?redirect=$urlOfThisPage");
    exit;
    }

    $nim = $_SESSION['user']['id'];

    $listKlsQuery = "SELECT * 
        FROM tb_kelas
        where kode_kelas NOT IN(
        SELECT kks.kelas FROM kontrak_kelas AS kks WHERE mahasiswa = '$nim'
        )";

    $listKlsQuery = $con->query($listKlsQuery);

    //periksa apakah query berhasil
    if ($listKlsResult) {
        die("Error in query: " . $con->error);
    }

    $listKelas = $listKlsResult->fetch_all(MYSQLI_ASSOC);

    //handle form
    if (!isset($_POST['kontrak_kls_baru'])) {
        $kodekelas = htmlspecialchars($_POST['kode_kelas']);
        $kodekontrak = '';
    }

    // mencari kode yang belum terpakai
    do {
        $kodeKontrak = code_generator(5, 'KKS');
        $checkPK = $conn->prepare("SELECT * FROM Kontrak_Kelas WHERE kode = ?");
        $checkPK->bind_param('s', $kodeKontrak);
        $checkPK->execute();
        $checkPKResult = $checkPK->get_result();

        if ($checkPKResult->num_rows > 0) {
            // Kode yang belum terpakai ditemukan, ulangi penghasilan kode baru
        }
    } while ($checkPK !== FALSE && $checkPK->num_rows > 0);

    // menambahkan data kontrak kuliah
    $insertRespon = $conn->query("INSERT INTO Kontrak_Kelas VALUES ('$kodeKontrak', '$nim', '$kodeKelas')");

    if ($insertRespon) {
        header("location: pembelajaran.php");
        exit;
    }

    if (last_query_error($conn)) $_SESSION['alert'] = last_query_error($conn);

// mengecek jika ada suatu peringatan (alert)
$alert = '';
if (isset($_SESSION['alert']) && $_SESSION['alert']) {
    $alert = array(
        'error' => $_SESSION['alert']['error'],
        'message' => $_SESSION['alert']['message']
    );
    $_SESSION['alert'] = '';
}
?>
<!DOCTYPE html>

<body>
    <?php if ($alert) : ?>
        <?php if ($alert['error']) : ?>
            <div class="alert alert-warning alert-dismissible fade show position-absolute top-2 start-50 translate-middle-x" role="alert">
                <?= $alert['message'] ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php else : ?>
            <div class="alert alert-success alert-dismissible fade show position-absolute top-2 start-50 translate-middle-x" role="alert">
                <?= $alert['message'] ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif; ?>
    <?php
        $alert = '';
    endif;
    ?>
    <main class="container-md p-5">
        <header class="mb-4">
            <h1>Kontrak Kelas</h1>
        </header>
        <form action="daftar.php?nim=<?= $nim ?>" method="post">
            <article class="mb-3">
                <label class="form-label" for="kodeKelas">Pilih Kelas :</label>
                <select class="form-select" name="kode_kelas" id="kodeKelas" required>
                    <option value="" selected disabled>-- Pilih Kelas --</option>
                    <?php if (sizeof($listKls) > 0) : ?>
                        <?php foreach ($listKls as $kls) : ?>
                            <option value="<?= $kls['kode_kelas'] ?>">Kelas <?= $kls['nm_kelas'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>


                </select>
                <div class="invalid-feedback">Pilih salah satu dari opsi yang tersedia.</div>
            </article>
            <article class="mb-3 d-flex gap-2">
                <button id="tambahBrg" type="submit" name="kontrak_kls_baru" class="btn btn-primary flex-fill d-flex align-items-center justify-content-center gap-1">
                    <span class="material-icons">add</span>
                    <span>Kontrak Kelas Baru</span>
                </button>
            </article>
        </form>
        <hr>
        <a href="pembelajaran.php" class="mt-3 btn btn-success d-flex align-items-center justify-content-center gap-1">
            <span class="material-icons">arrow_back</span>
            <span>Kembali</span>
        </a>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
</body>

</html>