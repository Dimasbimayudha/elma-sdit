<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location: ../index.php");
	exit;
}	
if ($_SESSION['level'] !== 'guru') {
	header("Location: ../index.php");
	exit;
}

require_once "../config/db.php";

// $listMatkul = query("SELECT * FROM mata_kuliah ORDER BY kode DESC LIMIT 12");
$listMatkul = query("SELECT * FROM tb_kelas INNER JOIN tb_guru ON tb_kelas.pengajar = tb_guru.id_guru");





if (isset($_POST['buat_matkul'])) {
	$kode = htmlspecialchars(strtoupper($_POST['kode_kelas']));
	$nama = htmlspecialchars($_POST['nama']);
	$dosen1 = htmlspecialchars($_POST['pengajar']);

	$stmt = $con->prepare("INSERT INTO tb_kelas (kode_kelas, nm_kelas, pengajar)
		VALUES (?, ?, ?)");
	$stmt->bind_param('sss', $kode, $nama,  $dosen1);

	try {
		if ($stmt->execute()) {
			$_SESSION['alert'] = array(
				'error' => FALSE,
				'message' => "Kelas baru berhasil ditambahkan."
			);
			header('location: kelas.php');
			exit;
		} else {
			// Handle duplicate entry error
			$duplicateKeyError = 1062;
			if ($stmt->errno === $duplicateKeyError) {
				$_SESSION['alert'] = array(
					'error' => TRUE,
					'message' => "Kode Kelas sudah terpakai! Harap gunakan kode lain."
				);
			} else {
				// Handle other execution errors
				handleExecutionError($con);
			}
		}
	} catch (Exception $e) {
		handleException($e, $con);
	}

	function handleExecutionError($con)
	{
		$duplicateKeyError = 1062;

		if ($con->errno === $duplicateKeyError) {
			$_SESSION['alert'] = array(
				'error' => TRUE,
				'message' => "Kode Mata Kuliah sudah terpakai! Harap gunakan kode lain."
			);
		} else {
			$_SESSION['alert'] = array(
				'error' => TRUE,
				'message' => "Terjadi kesalahan saat menambahkan data mata kuliah. Silakan coba lagi nanti."
			);
		}
	}
}
function handleException($exception, $con)
{
	$_SESSION['alert'] = array(
		'error' => TRUE,
		'message' => "Terjadi kesalahan saat menambahkan data mata kuliah. Silakan coba lagi nanti."
	);

	// Tambahkan penanganan eksepsi sesuai kebutuhan
}



$title = "Pembelajaran";
require_once "header.php";
require_once "navbar.php";
require_once "sidebar.php";



?>
<div id="layoutSidenav_content">
	<div class="container-fluid px-4">
		<h1 class="mt-4">Daftar Kelas</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="admin.php">Beranda</a></li>
				<li class="breadcrumb-item active" aria-current="page">Pembelajaran</li>
			</ol>
		</nav>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
			Tambah Data +
		</button>
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Kelas</h1>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form method="post" action="">
							<article class="mb-3">
								<label class="form-label" for="kodeMataKuliah">Kode Kelas :</label>
								<input class="form-control" type="text" name="kode_kelas" id="kodeMataKuliah" pattern="[A-Z]{2}[0-9]{3}" title="Masukkan 2 huruf kapital diikuti 3 digit angka" required>
								<div class="invalid-feedback">Kode Mata Kuliah tidak sesuai! (Contoh : RL201)</div>
								<div class="form-text">Kode Mata Kuliah terdiri dari 2 huruf kapital diikuti 3 digit angka.</div>
							</article>
							<article class="mb-3">
								<label class="form-label" for="namaMataKuliah">Nama Kelas :</label>
								<input class="form-control" type="text" name="nama" id="namaMataKuliah" required>
							</article>

							</article>
							<?php
							// query untuk mendapatkan semua data dosen yang ada
							$dosenResult = $con->query("SELECT id_guru, nm_guru FROM tb_guru");
							$listDosen = array();


							while ($dosen = $dosenResult->fetch_assoc()) {
								array_push($listDosen, $dosen);
							}
							?>
							<article class="mb-3">
								<label class="form-label" for="dosenPengampu1">Guru Pengajar :</label>
								<select class="form-select" name="pengajar" id="dosenPengampu1" required>
									<option value="" disabled>-- Pilih Pengajar --</option>
									<?php if (sizeof($listDosen) > 0) : ?>
										<?php foreach ($listDosen as $dosen) : ?>
											<?php if ($dosen['kode_kelas'] === $matkul['id_guru']) : ?>
												<option value="<?= $dosen['id_guru'] ?>" selected><?= $dosen['id_guru'] ?> - <?= $dosen['nm_guru'] ?></option>
											<?php else : ?>
												<option value="<?= $dosen['id_guru'] ?>"><?= $dosen['id_guru'] ?> - <?= $dosen['nm_guru'] ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
								<div class="invalid-feedback">Pilih salah satu dari opsi yang tersedia.</div>
							</article>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
						<button type="submit" name="buat_matkul" class="btn btn-primary">Simpan</button>
					</div>
					</form>

				</div>
			</div>
		</div>


		<div class="card mb-4">
			<div class="card-body">
				<table class="mt-3 table table-bordered table-striped table-hover">

					<?php if ($listMatkul && sizeof($listMatkul) > 0) : ?>



						<thead class="text-center">
							<tr>
								<th scope="col">Kode Kelas</th>
								<th scope="col">Nama Kelas</th>
								<th scope="col">Guru Pengajar</th>
							</tr>

						</thead>

						<tbody class="text-center">
							<?php foreach ($listMatkul as $pembelajaran) : ?>
								<tr class="mata-kuliah" data-link="pertemuan.php?kode=<?= $pembelajaran['kode_kelas'] ?>">
									<td data-label="Kode MK"><?= $pembelajaran['kode_kelas'] ?></td>
									<td data-label="Nama MK"><?= $pembelajaran['nm_kelas'] ?></td>

									<td data-label="Dosen Pengampu">
										<ul style="list-style-type: none;">
											<li><?= $pembelajaran['nm_guru'] ?></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
				</table>
			</div>
		<?php else : ?>
			<p class="p-3 border bg-light">Tidak ditemukan data mata kuliah apapun.</p>
		<?php endif; ?>
		</div>
	</div>

	<script>
		const matkulRows = document.querySelectorAll('.mata-kuliah');
		matkulRows.forEach((row) => {
			row.addEventListener('click', () => {
				window.location.href = row.getAttribute('data-link');
			})
		});
	</script>
	<script>
		var el = document.getElementById("wrapper");
		var toggleButton = document.getElementById("menu-toggle");

		toggleButton.onclick = function() {
			el.classList.toggle("toggled");
		};
	</script>


	<?php require_once "footer.php"; ?>