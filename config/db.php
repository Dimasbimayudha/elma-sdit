<?php

$serverName = "localhost";
$user = "root";
$pass = "";
$db = "register";

$con = mysqli_connect($serverName, $user, $pass, $db);

function query($query){
    global $con;
    $result = mysqli_query($con, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

if($con->connect_error){
    die("Connection Error: ". $con->connect_error);
}
