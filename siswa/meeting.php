<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location: ../index.php");
	exit;
}	
if ($_SESSION['level'] !== 'guru') {
	header("Location: ../index.php");
	exit;
}

require_once "../config/db.php";

// jika URL tidak valid (tidak memiliki parameter 'kode')
if (!$_GET['kelas'] || empty($_GET['kelas'])) {
	$_SESSION['alert'] = array(
	'error' => TRUE,
	'message' => "Url tidak valid."
	);
	header("location: kelas.php");
	exit;
}

// mengarahkan ke halaman login jika sesi login tidak aktif atau user tidak valid
if (!isset($_SESSION['login']) || $_SESSION['login'] || !isset($_SESSION['level']) || empty($_SESSION['level'])) {
	header("location: login.php?redirect-$urlofThisPage");
	exit;
}	

// mendapatkan kode kelas yang akan diakses
$kodekelas = $_GET['kelas'];

// daftar menu
$menus = array('Materi', 'Tugas');

// mendapatkan data menu yang ingin diakses pengguna
$menu =
(isset($_GET['menu']) && !empty($_GET['menu'])) ? $_GET['menu']: 'Materi';

// mendapatkan NIM dari user yang sedang login
$nim = $_SESSION['user']['id'];

// mendapatkan data pertemuan.
$pertemuanResult = query("SELECT * FROM tb_kelas INNER JOIN tb_guru ON tb_kelas.pengajar = tb_guru.id_guru where kode_kelas '$kodekelas' ");
$pertemuan = (sizeof($pertemuanResult) ===1) ? $pertemuanResult[0]: null;

//ika kode kelas tidak ditemukan
if (empty($pertemuan)) {
$_SESSION['alert'] = array(
'error' => true,
'message' => "<b>Kode Kelas tidak ditemukan.</b> Pastikan URL sudah benar"
);
}

//jika pertemuan tidak tersedia
if (!isset($pertemuan['is_exits']) || !$pertemuan['is_exist']) {
	$_SESSION['alert'] = array(
		'error' => true,
		'message' => 'Pertemuan tidak tersedia.'
	);
	} elseif (!$pertemuan['is_accessible']) {
	// jika pertemuan belum bisa diakses saat ini
	$waktu_akses = $pertemuan['accses_time'];
	$_SESSION['alert'] = array(
		'error' => true,
		'message' => "Pertemuan ke-$noPertemuan masih terkunci!<br>Anda baru dapat mengaksesnya pada tanggal <b>$waktu_akses</b>"
	);
}

$title = "Dasboard";
require_once "header.php";
require_once "navbar.php";
require_once "sidebar.php";
?>
<div id="layoutSidenav_content">
	<div class="container-fluid px-4">
		<h1 class="mt-4">Daftar Kelas</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="admin.php">Beranda</a></li>
				<li class="breadcrumb-item active" aria-current="page">Pembelajaran</li>
			</ol>
		</nav>

<?php if (isset($_SESSION['alert']) && lempty($_SESSION['alert'])): ?>
	<main>
		<section class="content">
			<a href="./dashboard.php" id="logoutBtn" class="btn primary-btn">
				<i class="material-icons-outlined">home</i>
				<span>Kembali ke Dashboard</span>
			</a>
		</section>
		<section>
			<div class="menus">
				<ul>
				<li class="sticky">
					<a href="./dashboard.php" class="menu icon-btn" title="Ke Dashboard">
					<i class="material-icons-outlined">arrow_back</i>
					</a>
				</li>
				<?php if (isset($menus) && !empty($menus)): ?>
					<?php foreach ($menus as $mn): ?>
						<?php if (strtoupper($mn) === strtoupper($menu)): ?>
							<li><a href="./meeting.php?kelas=<?= $kodekelas ?>&menu=<?= $mn ?>" class="menu active"><?= $mn ?></a><
						<?php else: ?>
						<li><a href="./meeting.php?kelas=<?= $kodekelas ?>&menu=<?= $mn ?>" class="menu"><?= $mn ?></a></li>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</section>

<?php if (isset($menu) && $menu === 'Materi'): ?>
	<section class="content" id="materi">
		<?php if (!empty($pertemuan['deskripsi'])): ?>
			<section class="desc">
				<article>
					<h3>Deskripsi Pertemuan</h3>
					<p><?= $pertemuan ['deskripsi'] ?></p>
				</article>
			</section>
		<?php endif; ?>
<?php

// kode pertemuan diambil dari $pertemuan['kode']
$kodeMeeting = $pertemuan['kode_kelas'];
// mendapatkan data materi
$materiResult = $conn->query("SELECT * FROM Materi WHERE pertemuan='$kodeMeeting'");

?>
<?php if ($materiResult && $materiResult->num_rows > 0): ?>
	<?php while ($materi = $materiResult->fetch_assoc()): ?>
	<article>
		<h3><?= $materi['judul'] ?></h3>
		<p><?= $materi['deskripsi'] ?></p>
		<?php if (strpos($materi[ 'mimetype'], 'image') !== FALSE): ?>
			<img src="../db/<?= $materi['nama_file'] ?>" alt="<?= $materi['nama_file'] ?>" width="50%" height="50%"
	<?php elseif (!empty($materi['nama_file'])): ?>
		<a href="../db/<?= $materi['nama_file'] ?>" target="_blank" class="btn">
			<i class="material-icons-outlined">attachment</i>
			Unduh File Materi
		</a>
<?php else : ?>
		<a href="<?= $materi['url'] ?>" target="_blank" class="btn">
			<i class="material-icons-outlined">link</i>
			Kunjungi URL
		</a>
		<?php endif; ?>
	</article>
<?php endwhile; ?>
<?php else: ?>
	<article>
		<p>Belum ada materi pada pertemuan ini.</p>
	</article>
<?php endif; ?>
		</section>
	<?php elseif (isset($menu) && $menu === 'Tugas'): ?>
		<section class="content" id="tugas">
	<?php
	// kode pertemuan diambil dari $pertemuan['kode']
	$kodeMeeting = $pertemuan['kode_kelas'];
	// mendapatkan data tugas
	$tugasResult = $con->query("SELECT * FROM tugas WHERE pertemuan='$kodeMeeting' ORDER BY deadline ASC, kode ASC");

	//menghandle form submit tugas
	if (isset($_POST['upload_tugas'])) {
		$noTugas = htmlspecialchars($_POST['no_tugas']);
		$kodeTugas = htmlspecialchars($_POST['kode_tugas']);
		$kodeSubmit = htmlspecialchars($_POST['kode_submit']);
		$nim= htmlspecialchars($_POST['nim_mhs']);

		$iskodeSubmitExist = $conn->query("SELECT EXISTS(SELECT kode_submit FROM submit_tugas WHERE kode_submit='$kodeSubmi");
		
		// jika sudah ada file yang dikumpulkan untuk tugas ini
		if ($iskodeSubmitExist && $isKodeSubmitExist->fetch_row()[0]) {
			echo "<p>Anda sudah mengumpulkan file untuk <b>Tugas $noTugas.</b> Silahkan hapus file lama jika ingin mengumpu"
		}else{

		// Fungsi untuk memproses nama file dan mendapatkan ekstensi
		function break_filename($file)
		{
		$filename = $file['name'];
		$parts = pathinfo($filename);
		$name = $parts['filename'];
		$ext = $parts['extension'];
		return ['filename' => $name, 'extension' => $ext];
		};

		// Fungsi untuk mengunggah file
		// Fungsi untuk mengunggah file
		// Fungsi untuk mengunggah file
		function upload_file($file, $destination)
		{
			// Pastikan direktori tujuan sudah ada
			if (Ifile_exists(dirname($destination))) {
				mkdir (dirname($destination), 0777, true);
		}

			// Periksa izin tulis pada direktori tujuan
			if (!is_writable(dirname($destination))) {
				return [
				'error' => true,
				'message' => 'Tidak dapat menulis ke direktori tujuan.'
				];
			}

			//memindahkan file 
			if (move_uploaded_file($file['tmp_name'],$destination)) {
				return [
					'error' => true,
					'message' => 'Gagagl mengunggah file.'
				];
			}
		}


		$fileTugas = $_FILES['file_tugas'];
		// mendapatkan nama file & ekstensinya
		$breakFileName = break_filename($fileTugas);
		$fileName = $breakFileName['filename'];
		$fileExt = $breakFileName['extension'];
		$newFileName = $kodeSubmit . '_' . $fileName . '_' . $fileExt; // nama file baru
		$fileDestination = './db/' . $newFileName; // lokasi tujuan penyimpanan file
		$uploadRespons = upload_file($fileTugas, $fileDestination);

		if ($uploadRespons['error']) {
			echo "<p>". SuploadRespons['message'] . "</p>";
			} else {
			$stmt = $con->prepare("INSERT INTO Submit_Tugas (kode_submit, mahasiswa, tugas, file_tugas, mimetype) VALL")
			$stmt ->bind_param('sssss', $kodeSubmit, $nim, $kodeTugas, $newfiletame, $fileTugas['type']);
			
			if ($stmt->execute()) {
			echo "<p>File untuk <b>Tugas SnoTugas</b> berhasil dikumpulkan.</p>";
			}
		}
	}
}

			// menghandle form hapus tugas
			if (isset($_POST['hapus tugas'])) {
			$kodeSubmit = $_POST['kode_submit'];
			$namaFile = $_POST['nama_file'];
			$nomorTugas = $_POST['nomor_tugas'];

			$deleteRespons = $con->query("DELETE FROM Submit_Tugas WHERE kode_submit='$kodeSubmit'");
				if (file_exists("./db/$namaFile")) {
					if (unlink("./db/$namaFile")) echo "<p>File yang Anda kumpulkan untuk <b>Tugas $nomor Tugas</b> berhasil ditambahkan"
		} else {
				//memberikan respon gagal 
				echo "<>Terjadi kesalahan saat menghapus data.<br><i>$con->error!</i>";
			}
}
?>
<?php if ($tugasResult && $tugasResult->num_rows >0) : ?>
	<?php $i = 1; ?>
	<?php while ($tugas = $tugasResult ->fetch_assoc()) : ?>
		<article>
			<?php
			//mendapatkakn waktu server saat ini 
			$getCurrentTime = $con->query("SELECT NOW()");
			$CurrentTime = strtotime($getCurrentTime->fetch_row()[0]);

			// mendapatkan waktu deadline
			$deadline = strtotime($tugas['deadline']);
			
			// menghitung sisa waktu yang tersisa
			$leftTime = $deadline - $currentTime;
			$dayLeft = floor($leftTime / (60* 60 * 24));
			$leftTime %= 60 * 60 * 24;
			$hoursLeft = floor($leftTime / (60* 60));
			$leftTime %= 60* 60;
			$minutesLeft = floor($leftTime / (60));
			$leftTime %= 60;

			//mendapatkan data submit tugas untuk tugas ini 
			$kodeTugas = $tugas [ 'kode'];
			$submitResult = $conn->query("SELECT FROM Submit_Tugas WHERE mahasiswa='$nim' && tugas='$kodeTugas'");
			$submitTugas ($submitResult && $submitResult->num_rows === 1) ? $submitResult->fetch_assoc(): null;

			?>
			<h3>Tugas <?= $i ?> <?= $tugas['judul '] ?></h3>
			<table>
				<tr>
					<th title="Batas Waktu Pengumpulan">Deadline</th>
					<td>
						<p><b><?= $tugas['deadline'] ?></b></p>
						<?php if ($leftTime >= 0): ?>
							<p><?= $dayLeft ?> hari <?= $hoursLeft ?> jam <?= $minutesLeft ?> menit dari sekarang</p>
						<?php else: ?>
							<p>0 hari e jam 0 menit dari sekarang</p>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
				<th>Deskripsi</th>
					<?php if (empty($tugas['deskripsi'])): ?>
						<td>-</td>
					<?php else: ?>
						<td><?= $tugas['deskripsi'] ?></td>
					<?php endif; ?>
				</tr>
				<tr>
				<th>Lampiran</th>
					<?php if (empty($tugas [ 'lampiran'])): ?>
						<td>-</td>
					<?php else: ?>
						<td><a href="./db/<?= $tugas['lampiran'] ?>" target="_blank"><?= $tugas['lampiran'] ?></a></td>
					<?php endif; ?>
				</tr>
				<form action="meeting.php?kelas=<?= $kodekelas ?>&menu-Tugas" method="post" enctype="multipart/form-data"
					<?php if (!empty($submitTugas)): ?>
						<tr>
							<th>Preview Tugas</th>
							<td><a href="./db/<?= $submitTugas['file_tugas']?>" target="_blank" rel="noopener noreferr></a></td>"
						</tr>
						<tr>
							<th>Status Pengumupulan</th>
							<td>Sudah Mengumpulkan</td>
						</tr>
					<?php else : ?>
						<tr>
							<th><label for="fileTugas">Upload Tugas</label></th>
							<td><input type="file" name="file_tugas" id="filetugas" require></td>
						</tr>
						<tr>
							<th>Preview Tugas</th>
							<td>File Tugas yang diupload akan muncul disini.</i></td>
						</tr>
						<tr>
							<th>Status Pengumpulan</th>
							<th>Belum Mengumpulkan</th>
						</tr>
					<?php endif; ?>
					<tr>
						<th>Tindakan</th>
						<td class=".action-btn">
							<input type="hidden" name="no_tugas" value="<?= $i ?>">
							<input type="hidden" name="nim_mhs" value="<?= $nim ?>">
							<input type="hidden" name="kode_tugas" value="<?= $tugas['kode'] ?>">
							<input type="hidden" name="kode_submit" value="<?= 'SMT' . substr($tugas['kode'], 3) ?>">
						<?php if (empty($submitTugas)): ?>
							<button type="submit" class="btn submit" name="upload_tugas" id="submitTask">Kumpulkan
						<?php else: ?>
							<form action="./meeting.php?kode=<?= $kodeMeeting ?>" method="post">
								<input type="hidden" name="kode_submit" value="<?= $submitTugas['kode_submit'] ?>">
								<input type="hidden" name="nama_file" value="<?= $submitTugas['file_tugas'] ?>">
								<input type="hidden" name="nomor_tugas" value="<?= $i ?>">
								<button type="submit" name="hapus_tugas" class="btn clear" id="deleteTask" onclick=
							</form>
						<?php endif; ?>
						</td>
					</tr>
					</form>
		</article>
		<?php $i++; ?>
	<?php endwhile; ?>
<?php else : ?>
	<article>
		<p>Belum Ada Tugas pada Pertemuan INI.</p>
	</article>
	<?php endif; ?>
		</section>
	</main>


	<?php endif; ?>
	<?php endif; ?>
	<script src="./script/navbar.js"></script>
	<script src="./script/meeting.js"></script>
	
	<?php require_once "footer.php"; ?>