<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location: ../index.php");
	exit;
}	
if ($_SESSION['level'] !== 'guru') {
	header("Location: ../index.php");
	exit;
}

require_once "../config/db.php";

//fungsi untuk mendapatkan url halaman saat ini 
function get_url_of_this_page()
{
    //mendapatkan protokol (http dan https)
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER('HTTPS') === 'on' ? 'https' : 'http'; 

	//mendapatkan host (contoh: www.example.com)
	$host = $_SERVER['HTTP_HOST'];

	//mendapatkan path dan query string (contoh: /path/to/page?param=velue)
	$url = $_SERVER['REQUEST_URL'];

	//menggabungkan semua informasi menjadi url lengkap 
	$fullurl = $protocol . ';//' . $host . $url;

	return $fullurl;
}

//mendapatkan url dari laman saat ini 
$urlOfThisPage = get_url_of_this_page();

//mengarahkan ke halaman login jika sesi login tidak aktif atau user tidak valid 
{
if (!isset($_SESSION['login']) || !$_SESSION['login'] || !isset($_SESSION['user']) || empty($_SESSION['user'])) 
	header("location: login.php?redirect=$urlOfThisPage");
exit;
}

//mendapatkan NIM dari user yang sedang login 
$nim = $_SESSION['user']['id'];

//mengambil daftar kelas yang dikontrak oleh user
$matkul = query("SELECT * FROM kontrak_kelas
INNER JOIN tb_kelas
ON tb_kelas.pengajar = tb_guru.id_guru
INNER JOIN tb_siswa
ON kontrak_kelas.mahasiswa = tb_siswa.id.siswa
where mahasiswa = $nim
ORDER BY tb_kelas.kode_kelas ASC ");

$title = "Dasboard";
require_once "header.php";
require_once "navbar.php";
require_once "sidebar.php";

?>

<div id="layoutSidenav_content">
	<div class="container-fluid px 4">
		<h1 class="mt-4">Daftar Kelas</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="admin.php">Beranda</a></li>
				<li class="breadcrumb-item active" aria-current="page">Pelajaran</li>
			</ol>

			<a href="daftar.php">Daftarkan Saya</a>
		</nav>

		<div class="card-mb-4">
			<div class="card-body">
				<table class="mt-3 table table-bordered table-striped table-hover">

				<?php if ($matkul && sizeof($matkul) > 0) : ?>
					
					<thead class="text-center bg-primary">
						<tr>
							<th scope="col">Kode Kelas</th>
							<th scope="col">Nama Kelas</th>
							<th scope="col">Pengajar</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php foreach ($matkul as $kelas) : ?>
						<tr class="mata-kuliah" data-link="meeting.php?kelas=<?= $kelas['kode_kelas'] ?>">
						<td data-label="Kode MK"><?= $kelas['kode_kelas']?></td>
						<td data-label="Nama MK"><?= $kelas['nm_kelas']?></td>
						<td data-label="Pengajar"><?= $kelas['nm_kelas']?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php else : ?>
				<p class="p-3 border bg-light">Tidak ditemukan data mata kuliah apapun.</p>
				<?php endif; ?>
		</div>
	</div>

	<script src=""></script>
	<script src=""></script>

	<script>
		const matkulRows = document.querySelectorAll('.mata_kuliah');
		matkulRows.forEach((row) => {
			row.addEventListener('click',() => {
			window.location.href = row.getAttribute('data-link');
		})
	});
	</script>
</div>